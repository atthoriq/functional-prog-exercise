# Functional Programming in Typescript with React and Redux
Context note: **React**

## MVC
### Traditional MVC

- MVC is popularized by Apple

### How it works

-
-

### How Component UI Works

- Get data untuk component melalui model
- If data being modified through UI, UI will re-render the whole component

## React
### What is React

- Dulunya library aja, sekarang jadi framework
- The V in MVC
- Component-based
- Berbasis deklaratif
- One-way data flow (component pass to their children)

### Components

- Declaration bisa class components atau functional components
- `ReactDOM.render` method creates a new component tree

### Component Lifecycle Methods
- Mounting:
    - componentWillMount (pas mau render)
    - componentDidMount (setelah render)
- Updation:
    - (1) componentWillReceiveProps (khusus props, updation states gausah)
    - (2) shouldComponentUpdate
    - (3) componentWillUpdate (run if (2) true)
    - (4) componentDidUpdate
- Unmounting:
    - componentWillUnmount

### State Management with Redux
- Actions, Reducers, Store
- Reduce take action