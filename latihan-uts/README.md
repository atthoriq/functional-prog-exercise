# Quiz Note(s)

#### Foldl Foldr (maxList)

```
maxList_ xs = foldl max 0 xs

foldl atau foldr have 3 parameters, function, initial value, list. initial value ini bakal diapply ke function sama first element in list. hasilnya, jadi value selanjutnya yg kemudian diapply ke function sama element berikutnya. initial value itu kaya accumulator.
```
contoh sum of array using foldl or foldr
```
sum_ xs = foldl (+) 0 xs
sum__ xs = foldr (+) 0 xs
```

#### Merge Sort
division operator `/` is only for fractional type. `div` is for integral (int or integer) type. (check :t)

#### tree.hs
```
*Main> let tri = treeInsert 2 EmptyTree
*Main> tri
Node 2 EmptyTree EmptyTree

*Main> let tri_ = treeInsert 1 tri
*Main> tri_
Node 2 (Node 1 EmptyTree EmptyTree) EmptyTree

*Main> let tri__ = treeInsert 2 tri_
*Main> tri__
Node 2 (Node 1 EmptyTree EmptyTree) EmptyTree
```

generateTree foldr foldl
```
generateTree :: (Ord a, Integral a) => [a] -> Tree a
-- generateTree_ :: (Ord a, Integral a) => [a] -> Tree a

generateTree arr = foldr treeInsert EmptyTree arr 
-- generateTree_ arr = foldl treeInsert EmptyTree arr 

generateTree_ ketika menggunakan foldl gabisa karena sebenarnya foldr atau foldl kerjanya:
foldr f acc list == dia mengeksekusi f untuk setiap element list dengan listofelements sebagai parameter pertama ( list `f` acc )

foldl f acc list == dia mengeksekusi f untuk setiap element list dengan acc sebagai parameter pertama ( acc `f` list )

jadi ketika foldr itu dia bisa karena fungsi :t treeInsert = Int a -> Tree a -> Tree a
ketika foldl itu gabisa karena jadinya kebalik, Tree jadi parameter pertama, elementnya kedua. (gak match dengan treeInsert nya)
```
##### References
- https://gist.github.com/CMCDragonkai/9f5f75118dda10131764