-- fpb kpk
fpb x 0 = x
fpb x y = fpb y (x `mod` y)
fpbl x y = [ z | z <- [(min x y) .. 1] , x `mod` z == 0, y `mod` z == 0]

kpk x y = take 1 ([z  | z <- [x, x*2 ..], z `mod` y ==0 ])
kpk_ x y = take 1 ([x*z | z <- [1..y], x*z `mod` y == 0 ])

-- maxList using FOLDR FOLDL
max_ x y 
    | x > y = x
    | otherwise = y

min_ x y
    | x < y = x
    | x == y = x
    | x > y = y -- bisa >2 condition

maxList [] = 0
maxList (x:xs) = max_ x (maxList xs)

maxList_ xs = foldl max 0 xs

-- mergesort
merge [] kanan                          = kanan
merge kiri []                           = kiri
merge kiri@(x:xs) kanan@(y:ys) | x < y  = x : merge xs kanan
                               | y <= x = y : merge kiri ys

mergeSort xs = merge (mergeSort kiri) (mergeSort kanan)
    where kiri  = take ((length xs) `div` 2) xs
          kanan = drop ((length xs) `div` 2) xs

-- myconcat implementation
concat_ [] = []
concat_ ([]:vs) = concat_ vs
concat_ ((x:xs):vs) = x:concat_ (xs:vs)

concat__ xs = foldl (++) [] xs