-- Kelas A Soal 1
isNumber x = x `notElem` "1234567890"
numberRemoval = filter isNumber

isVowel x = x `elem` "aiueoAIUEO"
countVowel = length . filter isVowel
isThreeOrLessVowels str = countVowel str < 4

-- ngilangin vowel >=4 dan remove number
removeNotCool lst = filter isThreeOrLessVowels (map numberRemoval lst)

weirdSementara = ((filter isThreeOrLessVowels) . (map numberRemoval))
removeNotThatCool lst = weirdSementara lst

isNotAppearTwiceInARow (a:b:ls) | (a==b) = False
isNotAppearTwiceInARow (a:ls) = isNotAppearTwiceInARow ls
isNotAppearTwiceInARow [] = True

weirdFilter = filter isThreeOrLessVowels
    . filter isNotAppearTwiceInARow
    . map numberRemoval

-- soal 2
rotabc = map abc
    where
        abc 'a' = 'b'
        abc 'b' = 'c'
        abc 'c' = 'a'
        abc x = x

-- soal 3
lastEl str = foldl1 (\x y -> y) str

lastEl3 :: [a] -> a
lastEl3 = foldl1 (\_ y -> y)

lastEl2 = head . reverse