listSum [] = 0
listSum (x:xs) = x + listSum xs

fold op init [] = init
fold op init (x:xs) = x `op` fold op init xs

listProd xs = fold (*) 1 xs

puter [] = []
puter xs = rev [] xs
    where rev acc  []    = acc
          rev acc (x:xs) = rev (x:acc) xs