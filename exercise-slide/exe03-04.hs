getLinee = 
    do c <- getChar        -- get a character
       if c == '\n'        -- if it’s a newline
          then return ""   -- then return empty string
          else do l <- getLinee 
                  return (c:l)  -- and return entire line
   
wcf :: (Int,Int,Int) -> String -> (Int,Int,Int)
wcf (ch,wo,li) [] = (ch,wo,li)
wcf (ch,wo,li) (' '  : xs) = wcf (ch+1,wo+1,li) xs
wcf (ch,wo,li) ('\t' : xs) = wcf (ch+1,wo+1,li) xs
wcf (ch,wo,li) ('\n' : xs) = wcf (ch+1,wo+1,li+1) xs
wcf (ch,wo,li) (x : xs)    = wcf (ch+1,wo,li) xs


wc :: IO ()
wc = do name <- getLine
        contents <- readFile name
        let (cc,w,lc) = wcf (0,0,0) contents
        putStrLn ("The file: " ++ name ++ "has ")
        putStrLn (show cc ++ " characters ")
        putStrLn (show w  ++ " words ")
        putStrLn (show lc ++ " lines ")