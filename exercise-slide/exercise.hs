myReverse :: [Int] -> [Int]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

data Day = Sun | Mon | Tue | Wed | Thu | Fri | Sat deriving Show
valday :: Integer -> Day
valday 1 = Sun
valday 2 = Mon

-- data Bool = True | False deriving Show
data Direction = North | East | South | West   deriving Show
data Move = Paper | Rock | Scissors   deriving Show

beats :: Move -> Move
beats Paper    = Scissors
beats Rock     = Paper
beats Scissors = Rock

-- Tagn :: Integer -> Tagger
-- Tagb :: Bool -> Tagger

data Tagger = Tagn Integer | Tagb Bool

number (Tagn n) = n
boolean (Tagb b) = b

isNum (Tagn _) = True
isNum (Tagb _) = False

----

data Temp = Celcius Float | Fahrenheit Float | Kelvin Float deriving Show

toKelvin (Celcius c) = Kelvin (c+272.0)
toKelvin (Kelvin k) = Kelvin k

----
data Shape = Rectangle Float Float
            | Ellipse Float Float
            | Pollygon [(Float, Float)] deriving Show

square :: Float -> Shape
area :: Shape -> Float

square side = Rectangle side side
area (Rectangle s1 s2) = s1*s2
area (Pollygon (v1:pts)) = polyArea pts
    where polyArea :: [(Float, Float)] -> Float
          polyArea (v2:v3:pts) = triarea v1 v2 v3 + polyArea (v3:pts)
          polyArea _ = 0

triarea v1 v2 v3 =
    let a = distBetween v1 v2
        b = distBetween v2 v3
        c = distBetween v3 v1
        s = 0.5*(a+b+c)
    in sqrt (s*(s-a)*(s-b)*(s-c))

distBetween (x1,y1) (x2,y2) = sqrt ((x1-x2)^2 + (y1-y2)^2) 




