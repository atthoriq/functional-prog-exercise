import Data.List

-- Contoh ListCompre and Lazy
-- nested list compre
nestedlist = [[(i,j) | i <- [1,2]] | j <- [1..]]
-- output: [[(1,1),(2,1)],[(1,2),(2,2)],[(1,3),(2,3)]]

notnested = [ (i,j) | i <- [1,2], j <- [1..] ]
-- output: [(1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10)]
-- 2 from i would never be executed since j is infinite

-- 01
no01 =  [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]

-- 02
pembagi n x = (n `mod` x == 0)
divisor n = [ x | x <- [1..n], pembagi n x ]
divisor2 n = [ x | x <- [1..n], n `mod` x == 0]

lebihkecillazy n = [ x | x <- [1 .. ], x < n]

-- quicksort
quicksort [] = []
quicksort (x:xs) = quicksort [ y | y <- xs, y <= x] ++ [x] ++ quicksort [y| y <- xs, y > x]

quick (x:xs) = [ y | y <- xs, y <= x]
quickfilterless (x:xs) = filter (<=x) xs
quickfiltergreat (x:xs) = filter (>x) xs

quickfilter p (x:xs) = filter p xs

quicksort2 [] = []
quicksort2 (x:xs) = quicksort2 (quickfilter (<=x) (x:xs)) ++ [x] ++ quicksort2 (quickfilter (>x) (x:xs))

-- somehow, this one didnt work
-- quicksort2 (x:xs) = quicksort2 (filter (<=x) (x:xs)) ++ [x] ++ quicksort2 (filter (>x) (x:xs))

-- permutation
substractlist = [2,3,4,5] \\ [3,4]

perm []  = [[]]
perm ls = [x:ps | x <- ls, ps <- perm(ls\\[x])]

permtry ls = [x:ps | x <- ls, ps <- [[2,3]]]
-- output: [[4,2,3],[5,2,3],[6,2,3]]
-- self explanatory, look below another example

permtry2 ls = [(x,ps) | x <- ls, ps <- [[2,3]]]
-- output: [(4,[2,3]),(5,[2,3]),(6,[2,3])]
-- every element on ls be paired up with ps. note: ps here is only one element (one list, in one list)

-- prime number
-- the idea, I have list [2..], i want to eliminate number from [3..] which [3..] modulo 2 == 0 
-- then, i have the result after first elmination.let say x:xs. 
-- then, i want to eliminate number from xs=[m..] whcih [m..] modulo x == 0
-- it runs until end of the day

prime = sieve [2..]
    where sieve (x:xs) = x : (sieve [ y | y <- xs, y `mod` x /= 0])

-- Triple Pythagoras
pythago = [(i,j,k) | i <- [3..], j <- [4..], k <- [5..], i*i + j*j == k*k]

trytriple = [(i,j,k) | i <- [3..], j <- [4..], k <- [5..]]
-- output: [(3,4,5),(3,4,6),(3,4,7),(3,4,8),(3,4,9),(3,4,10),(3,4,11),(3,4,12),(3,4,13),(3,4,14)]
-- persis kaya forloop nested 3 kali. karena infinite, k nya ga selesai2.
-- makanya pythago di atas cuma keluar (3,4,5) karena i sama j nya gak increase
-- NOTE: K nya diloop paling bawah karena urutannya i j k

pythago2  = [(i,j,k) | k <- [5..], i <- [3, 2 .. 1], j <- [4, 3 ..1]]
-- kalo kaya gini, k nya bisa increase tapi tetep infinit dan batas i sama j nya cuma dikit.
-- but the idea is, k nya bisa increase karena i sama j nya terbatas

pythago3 = [(i,j,k) | k <- [5..], i <- [k, k-1 .. 1], j <- [k, k-1 .. 1], i*i + j*j == k*k]
-- [(4,3,5),(3,4,5),(8,6,10),(6,8,10),(12,5,13)]
-- kaya gini bisa muncul pythagorasnya!
-- tapi, look closer, ketika k nya = 5, ada kemungkinan antara i=4, j=3 atau i=3, j=4.
-- bisa gitu karena ada kasus dimana i atau j lebih besar satu sama lain. jadi bisa i=4 j=3 k=K
-- makanya, salah satu dari i atau j harus ngiket yg lain biar gak lebih besar satu sama lain

pythago4 = [(i,j,k) | k <- [5..], i<- [k,k-1 .. 1], j <- [i, i-1 .. 1], i*i + j*j == k*k]
-- berhasil