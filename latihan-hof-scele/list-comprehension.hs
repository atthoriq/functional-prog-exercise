-- PART 02
-- 01
-- xs: [1,2,3]
plusone xs = [ x+1 | x <- xs]
plusonemap xs = map (+1) xs

-- 02
-- xs, ys:  [1,2,3]
no2 xs ys = [x+y | x <- xs, y <- ys]

-- cartProd xs ys = (,) <$> xs <*> ys
-- http://learnyouahaskell.com/functors-applicative-functors-and-monoids#applicative-functors
assignOpToList xs = fmap (+) xs
cartProd xs ys = assignOpToList xs <*> ys

no2map xs ys = concat (map (\x -> map (\y -> (x+y)) ys) xs)
no2withoutconcat xs ys = map (\x -> map (\y -> (x+y)) ys) xs

-- 03
no3 xs = [ x+2 | x <- xs, x > 3]

no3map xs = map (+2) (filter (>3) xs)

-- 04
no4 xys = [ x+3 | (x,_) <- xys ]

no4map xys = map (+3) (map fst xys)

-- 05
no5 xys = [ x+4 | (x,y) <- xys, x+y < 5 ]

xyless5 (x,y)
    | x+y < 5 = True
    | otherwise = False

no5map xys = map (+4) (map fst (filter (xyless5) xys))

-- 06
-- mxs: [(Just x), (Just y)]
no6 mxs = [ x+5 | Just x <- mxs ]

no6f (Just x) = x+5
no6map mxs = map (no6f) mxs

-- 07
no7map xs = map (+3) xs

no7 xs = [x+3 | x <- xs]

-- 08
no8filter xs = [x | x <- xs, x > 7]

-- 09
no9map xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

no9 xs ys = [(x,y) | x <- xs, y <- ys]

-- 10
no10map xys = filter (>3) (map (\(x,y) -> x+y) xys)

no10 xys = [x+y | (x,y) <- xys , x+y > 3]