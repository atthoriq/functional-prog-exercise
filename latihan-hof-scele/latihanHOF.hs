panjang [] = 0
panjang (x:xs) = 1 + panjang xs

ganti [] = []
ganti (x:xs) = 1 : ganti xs
penjeng xs = sum (ganti xs)

-- 01
satu x = 1
ponjong xs = sum (map satu xs)

-- 03
iter 0 f x = x
iter n f x = iter (n-1) f (f x)

-- 04 it behaves like regular iter but specifically for succ function
-- suck = \x -> x+1
anotheriter :: (Num a, Enum a, Eq a) => a -> a -> a
anotheriter = \n x -> iter n succ x

-- 05 sum of square using map
-- not using map
sumofsquare 1 = 1
sumofsquare n = (sqrt n) + sumofsquare (n-1)

-- using map and sum
mapsumofhelper xs = sum (map sqrt xs)
mapsumof n = mapsumofhelper [1..n]

-- sum by fold
sumfold xs = foldr (+) 0 xs

-- map and sum by fold
mapsumfoldhelper xs = foldr (+) 0 (map sqrt xs)
mapsumfold n = mapsumfoldhelper [1..n]

-- 06 mystery
mystery xs = foldr (++) [] (map sing xs)
    where
        sing x = [x]

-- 07 id is polymorphic
aidi x = x

f :: Int -> Bool
f 0 = False
f n = True
-- 08
flip' :: (a -> b -> c) -> (b -> a -> c)
flip' g x y = g y x

-- PART 02
-- 01
plusone xs = [ x+1 | x <- xs]
plusonemap xs = map (+1) xs

-- 02
no2 xs ys = [x+y | x <- xs, y <- ys]

-- cartProd xs ys = (,) <$> xs <*> ys
-- http://learnyouahaskell.com/functors-applicative-functors-and-monoids#applicative-functors
assignOpToList xs = fmap (+) xs
cartProd xs ys = assignOpToList xs <*> ys

no2map xs ys = concat (map (\x -> map (\y -> (x+y)) ys) xs)

-- 03
no3 xs = [ x+2 | x <- xs, x > 3]

no3map xs = map (+2) (filter (>3) xs)

-- 04
no4 xys = [ x+3 | (x,_) <- xys ]

no4map xys = map (+3) (map fst xys)

-- 05
no5 xys = [ x+4 | (x,y) <- xys, x+y < 5 ]

xyless5 (x,y)
    | x+y < 5 = True
    | otherwise = False

no5map xys = map (+4) (map fst (filter (xyless5) xys))

-- 06
no6 mxs = [ x+5 | Just x <- mxs ]

no6f (Just x) = x+5
no6map mxs = map (no6f) mxs

-- 07
no7map xs = map (+3) xs

no7 xs = [x+3 | x <- xs]

-- 08
no8filter xs = [x | x <- xs, x > 7]

-- 09
no9map xs ys = concat (map (\x -> map (\y -> (x,y)) ys) xs)

no9 xs ys = [(x,y) | x <- xs, y <- ys]

-- 10
no10map xys = filter (>3) (map (\(x,y) -> x+y) xys)

no10 xys = [x+y | (x,y) <- xys , x+y > 3]